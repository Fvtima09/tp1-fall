package com.example.tp1_fall;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import data.Country;


public class DetailFragment extends Fragment {
    public static final String TAG = "DetailFragment";

    ImageView drapeau;
    TextView nom_pays;
    TextView capitale;
    TextView langue;
    TextView monnaie;
    TextView population;
    TextView superficie;




    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }




    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



       drapeau = view.findViewById(R.id.item_drapeau);
        nom_pays = view.findViewById(R.id.item_pays);
        capitale = view.findViewById(R.id.item_capitale);
        langue = view.findViewById(R.id.item_langue);
        monnaie =view.findViewById(R.id.item_monnaie);
        population = view.findViewById(R.id.item_population);
        superficie = view.findViewById(R.id.item_superficie);

        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());

        int i=args.getCountryId();


        String uri = Country.countries[i].getImgUri();
        Context c = drapeau.getContext();

        drapeau.setImageDrawable(c.getResources().getDrawable(
                c.getResources(). getIdentifier (uri , null , c.getPackageName())));

        nom_pays.setText(Country.countries[i].getName());
        capitale.setText(Country.countries[i].getCapital());
        langue.setText(Country.countries[i].getLanguage());
        monnaie.setText(Country.countries[i].getCurrency());
        population.setText(Integer.toString(Country.countries[i].getPopulation()));
        superficie.setText(Integer.toString(Country.countries[i].getArea()));









        view.findViewById(R.id.button_retour).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_DetailFragment_to_ListFragment);
            }
        });
    }
    }
